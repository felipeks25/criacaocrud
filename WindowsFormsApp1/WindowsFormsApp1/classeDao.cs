﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using System.Data.SqlTypes;
using System.Windows.Forms;

namespace WindowsFormsApp1 {
    class classeDao {
        
        private NpgsqlConnection conn = new NpgsqlConnection("Server = localhost; Port = 5432; User Id = postgres; Password = fe92020700; Database = Alunos");
        NpgsqlCommand command = null;
        //Estabelece Ligações a Bases de Dados
        public DataTable Pesquisar()
        {

            DataTable dt = null;
            try
            {
                command  = new NpgsqlCommand(" SELECT aluno.matricula, aluno.nome, aluno.Nascimento, aluno.telefone, notas.prova1, notas.prova2, notas.provaFinal " +
                                              "from aluno inner join notas on aluno.matricula = notas.matricula ORDER BY aluno.matricula", conn);
                NpgsqlDataAdapter da = new NpgsqlDataAdapter();
                da.SelectCommand = command;
                dt =  new DataTable();
                da.Fill(dt);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro de ligação");
            }
            return dt;
        }
        public void salvar(String matricula, String nome, String nascimento, String telefone, String prova1, String prova2, String provafinal) {
            command = new NpgsqlCommand("INSERT INTO ALUNO (MATRICULA, NOME, NASCIMENTO, TELEFONE) VALUES (@matricula, @nome, @nascimento, @telefone)", conn);
            
            try
            {
                command.Parameters.Add("@matricula", NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(matricula);
                command.Parameters.Add("@nome", NpgsqlTypes.NpgsqlDbType.Varchar).Value = nome;
                command.Parameters.Add("@nascimento", NpgsqlTypes.NpgsqlDbType.Date).Value = Convert.ToDateTime(nascimento);
                command.Parameters.Add("@telefone", NpgsqlTypes.NpgsqlDbType.Varchar).Value = telefone;      
                conn.Open();
                command.ExecuteNonQuery();
               
                    
               command = new NpgsqlCommand("INSERT INTO NOTAS (MATRICULA, Prova1, Prova2, ProvaFinal) VALUES (@matricula, @prova1, @prova2, @provafinal)", conn);
               command.Parameters.Add("@prova1", NpgsqlTypes.NpgsqlDbType.Numeric).Value = Convert.ToDecimal(prova1);
               command.Parameters.Add("@prova2", NpgsqlTypes.NpgsqlDbType.Numeric).Value = Convert.ToDecimal(prova2);
               command.Parameters.Add("@provafinal", NpgsqlTypes.NpgsqlDbType.Numeric).Value = Convert.ToDecimal(provafinal);
               command.Parameters.Add("@matricula", NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(matricula);                    
               command.ExecuteNonQuery();
                MessageBox.Show("Cadastrado com sucesso!");  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }
        public void remover(string matricula){
            ClassBll bll = new ClassBll();
            command = new NpgsqlCommand("DELETE from notas where matricula = @matricula", conn);
            
            try
            {
                command.Parameters.Add("@matricula", NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(matricula);
                conn.Open();
                command.ExecuteNonQuery();

                command = new NpgsqlCommand("DELETE from aluno where matricula = @matricula", conn);
                command.Parameters.Add("@matricula", NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(matricula);              
                command.ExecuteNonQuery();
                bll.exibirDados();
                MessageBox.Show("Aluno apagado");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public void Editar(String matricula, String nome, String nascimento, String telefone, String prova1, String prova2, String provafinal)
        {
            command = new NpgsqlCommand("UPDATE ALUNO SET NOME = @nome, NASCIMENTO = @nascimento, TELEFONE = @telefone " +
                                       " WHERE MATRICULA = @matricula", conn);

            try
            {
                command.Parameters.Add("@matricula", NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(matricula);
                command.Parameters.Add("@nome", NpgsqlTypes.NpgsqlDbType.Varchar).Value = nome;
                command.Parameters.Add("@nascimento", NpgsqlTypes.NpgsqlDbType.Date).Value = Convert.ToDateTime(nascimento);
                command.Parameters.Add("@telefone", NpgsqlTypes.NpgsqlDbType.Varchar).Value = telefone;
                conn.Open();
                command.ExecuteNonQuery();


                command = new NpgsqlCommand("UPDATE NOTAS SET  Prova1 = @prova1, Prova2 = @prova2, ProvaFinal = @provafinal " +
                                            " WHERE MATRICULA = @matricula", conn);
                command.Parameters.Add("@prova1", NpgsqlTypes.NpgsqlDbType.Numeric).Value = Convert.ToDecimal(prova1);
                command.Parameters.Add("@prova2", NpgsqlTypes.NpgsqlDbType.Numeric).Value = Convert.ToDecimal(prova2);
                command.Parameters.Add("@provafinal", NpgsqlTypes.NpgsqlDbType.Numeric).Value = Convert.ToDecimal(provafinal);
                command.Parameters.Add("@matricula", NpgsqlTypes.NpgsqlDbType.Integer).Value = Convert.ToInt32(matricula);
                command.ExecuteNonQuery();
                MessageBox.Show("Editado com sucesso!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }
    }
    
}
