﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class ClassBll
    {
        classeDao dao = new classeDao();

        public DataTable exibirDados(){
            DataTable dataT = new DataTable();
            try {
               
                dataT = dao.Pesquisar();
                
            }
            catch (Exception ex) {
                MessageBox.Show("Erro de busca", ex.Message);
            }
            return dataT;
        }
        public void cadastro(String matricula, String nome, String nascimento, String telefone, String prova1, String prova2, String provafinal) {
            dao.salvar ( matricula,  nome,  nascimento,  telefone,  prova1,  prova2,  provafinal);
        }
        public void apagar(String matricula) {
            dao.remover(matricula);
        }
        public void alterar(String matricula, String nome, String nascimento, String telefone, String prova1, String prova2, String provafinal)
        {
            dao.Editar(matricula, nome, nascimento, telefone, prova1, prova2, provafinal);
        }

    }
}
