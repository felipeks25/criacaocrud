﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.produtoLbl = new System.Windows.Forms.Label();
            this.ValidadeLbl = new System.Windows.Forms.Label();
            this.qtdLbl = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtNascimento = new System.Windows.Forms.TextBox();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.dtPesquisa = new System.Windows.Forms.DataGridView();
            this.btnCadastro = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnRemover = new System.Windows.Forms.Button();
            this.lblNome = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProva1 = new System.Windows.Forms.TextBox();
            this.txtProva2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtProvaFinal = new System.Windows.Forms.TextBox();
            this.btnEditar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMatricula = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtPesquisa)).BeginInit();
            this.SuspendLayout();
            // 
            // produtoLbl
            // 
            this.produtoLbl.AutoSize = true;
            this.produtoLbl.Location = new System.Drawing.Point(17, 47);
            this.produtoLbl.Name = "produtoLbl";
            this.produtoLbl.Size = new System.Drawing.Size(10, 13);
            this.produtoLbl.TabIndex = 0;
            this.produtoLbl.Text = " ";
            // 
            // ValidadeLbl
            // 
            this.ValidadeLbl.AutoSize = true;
            this.ValidadeLbl.BackColor = System.Drawing.SystemColors.Control;
            this.ValidadeLbl.Location = new System.Drawing.Point(17, 90);
            this.ValidadeLbl.Name = "ValidadeLbl";
            this.ValidadeLbl.Size = new System.Drawing.Size(66, 13);
            this.ValidadeLbl.TabIndex = 1;
            this.ValidadeLbl.Tag = "lblNascimento";
            this.ValidadeLbl.Text = "Nascimento:";
            // 
            // qtdLbl
            // 
            this.qtdLbl.AutoSize = true;
            this.qtdLbl.Location = new System.Drawing.Point(18, 127);
            this.qtdLbl.Name = "qtdLbl";
            this.qtdLbl.Size = new System.Drawing.Size(55, 13);
            this.qtdLbl.TabIndex = 2;
            this.qtdLbl.Tag = "lblTelefone";
            this.qtdLbl.Text = "Telefone: ";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(93, 47);
            this.txtNome.MaxLength = 500;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(586, 20);
            this.txtNome.TabIndex = 3;
            this.txtNome.Tag = "txtNome";
            // 
            // txtNascimento
            // 
            this.txtNascimento.Location = new System.Drawing.Point(93, 90);
            this.txtNascimento.MaxLength = 500;
            this.txtNascimento.Name = "txtNascimento";
            this.txtNascimento.Size = new System.Drawing.Size(132, 20);
            this.txtNascimento.TabIndex = 4;
            this.txtNascimento.Tag = "txtNascimento";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(93, 123);
            this.txtTelefone.MaxLength = 500;
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(98, 20);
            this.txtTelefone.TabIndex = 5;
            this.txtTelefone.Tag = "txtTelefone";
            // 
            // dtPesquisa
            // 
            this.dtPesquisa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtPesquisa.Location = new System.Drawing.Point(21, 217);
            this.dtPesquisa.Name = "dtPesquisa";
            this.dtPesquisa.RowTemplate.Height = 28;
            this.dtPesquisa.Size = new System.Drawing.Size(757, 310);
            this.dtPesquisa.TabIndex = 6;
            this.dtPesquisa.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtPesquisa_CellContentClick);
            // 
            // btnCadastro
            // 
            this.btnCadastro.Location = new System.Drawing.Point(136, 564);
            this.btnCadastro.Name = "btnCadastro";
            this.btnCadastro.Size = new System.Drawing.Size(101, 35);
            this.btnCadastro.TabIndex = 7;
            this.btnCadastro.Tag = "btnCadastra";
            this.btnCadastro.Text = "Cadastrar";
            this.btnCadastro.UseVisualStyleBackColor = true;
            this.btnCadastro.Click += new System.EventHandler(this.cadastroBtn_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(254, 564);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(103, 35);
            this.btnPesquisar.TabIndex = 8;
            this.btnPesquisar.Tag = "btnPesquisar";
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.pesquisarBtn_Click);
            // 
            // btnRemover
            // 
            this.btnRemover.Location = new System.Drawing.Point(493, 564);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(96, 35);
            this.btnRemover.TabIndex = 9;
            this.btnRemover.Tag = "btnRemover";
            this.btnRemover.Text = "Remover";
            this.btnRemover.UseVisualStyleBackColor = true;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(20, 47);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(35, 13);
            this.lblNome.TabIndex = 10;
            this.lblNome.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(218, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 11;
            this.label1.Tag = "lblProva1";
            this.label1.Text = "1° Prova ";
            // 
            // txtProva1
            // 
            this.txtProva1.Location = new System.Drawing.Point(215, 182);
            this.txtProva1.MaxLength = 500;
            this.txtProva1.Name = "txtProva1";
            this.txtProva1.Size = new System.Drawing.Size(76, 20);
            this.txtProva1.TabIndex = 12;
            this.txtProva1.Tag = "txtTelefone";
            // 
            // txtProva2
            // 
            this.txtProva2.Location = new System.Drawing.Point(327, 182);
            this.txtProva2.MaxLength = 500;
            this.txtProva2.Name = "txtProva2";
            this.txtProva2.Size = new System.Drawing.Size(76, 20);
            this.txtProva2.TabIndex = 13;
            this.txtProva2.Tag = "txtTelefone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(331, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 14;
            this.label2.Tag = "lblProva2";
            this.label2.Text = "2° Prova ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(442, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 15;
            this.label3.Tag = "lblProvaFinal";
            this.label3.Text = "Prova Final";
            // 
            // txtProvaFinal
            // 
            this.txtProvaFinal.Location = new System.Drawing.Point(439, 182);
            this.txtProvaFinal.MaxLength = 500;
            this.txtProvaFinal.Name = "txtProvaFinal";
            this.txtProvaFinal.Size = new System.Drawing.Size(76, 20);
            this.txtProvaFinal.TabIndex = 16;
            this.txtProvaFinal.Tag = "txtTelefone";
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(376, 564);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(101, 35);
            this.btnEditar.TabIndex = 17;
            this.btnEditar.Tag = "btnEditar";
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 18;
            this.label4.Tag = "lblMatricula";
            this.label4.Text = "Matricula";
            // 
            // txtMatricula
            // 
            this.txtMatricula.Location = new System.Drawing.Point(93, 16);
            this.txtMatricula.MaxLength = 500;
            this.txtMatricula.Name = "txtMatricula";
            this.txtMatricula.Size = new System.Drawing.Size(98, 20);
            this.txtMatricula.TabIndex = 19;
            this.txtMatricula.Tag = "txtMatricula";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(789, 609);
            this.Controls.Add(this.txtMatricula);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.txtProvaFinal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProva2);
            this.Controls.Add(this.txtProva1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.btnRemover);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.btnCadastro);
            this.Controls.Add(this.dtPesquisa);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.txtNascimento);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.qtdLbl);
            this.Controls.Add(this.ValidadeLbl);
            this.Controls.Add(this.produtoLbl);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dtPesquisa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label produtoLbl;
        private System.Windows.Forms.Label ValidadeLbl;
        private System.Windows.Forms.Label qtdLbl;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtNascimento;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.DataGridView dtPesquisa;
        private System.Windows.Forms.Button btnCadastro;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProva1;
        private System.Windows.Forms.TextBox txtProva2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtProvaFinal;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMatricula;
    }
}

