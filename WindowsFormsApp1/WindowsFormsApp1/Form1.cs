﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Npgsql;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        ClassBll conexao = new ClassBll();
        public Form1()
        {
            InitializeComponent();
  
         

        }

        private void pesquisarBtn_Click(object sender, EventArgs e)
        {
            
            try {
                dtPesquisa.DataSource = conexao.exibirDados(); 
            }
            catch (Exception ex) {
                MessageBox.Show("Erro: ",ex.Message);
            }
        }

        private void cadastroBtn_Click(object sender, EventArgs e)
        {
            

            conexao.cadastro(txtMatricula.Text,txtNome.Text,txtNascimento.Text,txtTelefone.Text,txtProva1.Text,txtProva2.Text,txtProvaFinal.Text);

        }

        private void dtPesquisa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) {
                DataGridViewRow row = this.dtPesquisa.Rows[e.RowIndex];

                txtMatricula.Text = row.Cells["matricula"].Value.ToString();
                txtNome.Text = row.Cells["Nome"].Value.ToString();
                txtNascimento.Text = row.Cells["Nascimento"].Value.ToString();
                txtTelefone.Text = row.Cells["Telefone"].Value.ToString();
                txtProva1.Text = row.Cells["Prova1"].Value.ToString();
                txtProva2.Text = row.Cells["Prova2"].Value.ToString();
                txtProvaFinal.Text = row.Cells["ProvaFinal"].Value.ToString();
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            conexao.apagar(txtMatricula.Text);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            conexao.alterar(txtMatricula.Text, txtNome.Text, txtNascimento.Text, txtTelefone.Text, txtProva1.Text, txtProva2.Text, txtProvaFinal.Text);
        }
    }
}
